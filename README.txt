
### Installation
Run 
'composer require jaybizzle/crawler-detect 1.*' 
or add 
"jaybizzle/crawler-detect" :"1.*" 
to your composer.json and use https://www.drupal.org/project/composer_manager.

### Usage
Go to admin/people/login-by-ip

### Optional - hook_user_login usage
/**
 * Implementation of hook_user_login()
 */
function CUSTOM_MODULE_user_login($edit, $account) {
  # Run custom calls if Login by IP is used
  if($account->data['login_by_ip_hook_user_login']) {

    # Your custom function calls go here
    # E.g. make calls to Salesforce

    // Set this back to FALSE so that the above code runs
    // only when Login by Ip module is used.
    $e['data']['login_by_ip_hook_user_login'] = FALSE;

    // Save user
    user_save($account, $e);
  }
}
