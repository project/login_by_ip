(function ($) {

  Drupal.behaviors.loginByIPExpCol = {
    attach: function(context, settings) {
      // Admin form helper functionality
      $("#login-by-ip-assigned-users .delete-user").click(function(event) {
        return confirm("Are you sure you want to delete this user IP?");
      });

      // Initially hide all IP lists
      $('#login-by-ip-assigned-users table #ip-column').hide();

      // Toggle the display of individual IP lists
      $("#login-by-ip-assigned-users table #toggle-ip-list").click(function() {
        $(this).parent().find("#ip-column").slideToggle("fast");
      });
    }
  };

})(jQuery);
