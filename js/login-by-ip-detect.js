(function ($) {

  Drupal.behaviors.loginByIPDetect = {
    attach: function(context, settings) {

    var basePath = Drupal.settings.basePath;
    var random = Math.random().toString(36).substring(2, 36) + Math.random().toString(36).substring(2, 36);
    var callPath = basePath + "login_by_ip_detect_visitor/";

    // Use POST method so that URL calls are not cached by any external agent
    $.post(callPath, function(userIP) {
      // If TRUE, start the login process
      if(userIP) {
        // Display a message while the client waits
        $('#footer > #block-menu-menu-footer').after('<center style="color:#cccccc;">'+userIP+'</center>');
      }
    });

    }
  };

})(jQuery);
