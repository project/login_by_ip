// Shorthand for $(document).ready(). See http://api.jquery.com/ready for further details.
(function($) {
  // Prevent colorbox from interfering
	// https://github.com/jackmoore/colorbox/issues/389
  if($.colorbox) {
    $.colorbox.settings.trapFocus = false;
    $.colorbox.remove();
  }

	var basePath = Drupal.settings.basePath;
  var site_frontpage = Drupal.settings.login_by_ip.site_frontpage;
  var checkInterval = Drupal.settings.login_by_ip.checkInterval;
  console.log('checkInterval', checkInterval);

  var date = new Date();
  var currentTime = date.getTime();
  console.log('currentTime', currentTime);
  var cookieExpire = date.setTime(currentTime + (checkInterval * 1000));
  console.log('cookieExpire', cookieExpire);

  /////////// The following code sequence is in case an external cache is used (e.g. Varnish) ///////////
  var cookieCheck = $.cookie('login_by_ip_access');
  /*
  if(cookieCheck && cookieCheck >= currentTime) {
    console.log('got cookie', cookieCheck);
    // Make sure to reload the client's requested page.
    location = window.location;
    // By default, the reload() method reloads the page from the cache, but you can force it to
    // reload the page from the server by setting the forceGet parameter to true: location.reload(true).
    location.reload(true);
  } else {
    */
    console.log('NO cookie or NOT in window', cookieCheck);
    // This run the visitor IP check on every page except login, logout and registration pages.
    if(window.location.pathname != '/user/login' && window.location.pathname != '/user/logout' && window.location.pathname != '/user/register') {
      $.cookie('login_by_ip_access', cookieExpire, { path: '/' });
      // Check if a user is assigned to the current client IP.
      // First, build the call request to check the client's IP.
      var callPath = basePath + "login_by_ip_is_user";

      // Use POST method so that URL calls are not cached by any external caching agent (e.g. Varnish)
      $.post(callPath, function(userInfo) {
        // If TRUE, start the login process
        if(userInfo) {
          // Display a message while the client waits
          $('body > #page > #main > #content').prepend('<div class="messages status">Your IP is linked to a subscription account. Please wait while we log you in...</div>');
          // Wait 5 seconds for the user to read the above message.
          setTimeout(function() { }, 5000);

          // Make sure to reload the client's requested page.
          location = window.location;
          // By default, the reload() method reloads the page from the cache, but you can force it to
          // reload the page from the server by setting the forceGet parameter to true: location.reload(true).
          location.reload(true);
        }
      });
    }
  // }

})(jQuery);
